# README #

### What is this repository for? ###

* These are some quick helper functions when deveopling in JQuery / Javascript
* Version 1.5

### How do I get set up? ###


* Required - Install Latest [JQuery](https://jquery.com/download/) inside "/assets/js/" or whatever folder you use for JS
* Install [bootstrap.min.js](https://getbootstrap.com/docs/4.1/getting-started/download/) inside "/assets/js/" if you add AlertMessage.js or whatever folder you use for JS
* Install Util.js inside "/assets/js/" or whatever folder you use for JS
* Then link the scripts in HTML like below.

### HTML Preview ###

```
	<!DOCTYPE html>
	<html>
	<head>
		<script type="text/javascript" src="/assets/js/JQuery.min.js"></script>
		<script type="text/javascript" src="/assets/js/Util.js"></script>
		<!-- Only Include if using ALertMessage.js-->
		<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
	</head>
		<body>
		</body>
	</html>
```


### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact