/**  Useful JQUERY Utilities */
//------------------------------
//  - Ron Rebennack
//  - Bradley Worrell-Smith
//  - Derek Piper
//  - Christian Preston

ResultSet = {
    STAT_GOOD: "GOOD",
    STAT_INFO: "INFO",
    STAT_ERROR: "ERROR",
    STAT_NODATA: "NODATA",
    STAT_PARTIAL: "PART",
    STAT_WARNING: "WARNING",
    STAT_INVALID: "INVALID",
    Good: "GOOD",
    Error: "ERROR"
};

Util = new (function () {
    var _ie_ver = null;
    var _log_enabled = true;

    this.IEVersion = function ()
    {
        if (_ie_ver == null) {
            var myNav = navigator.userAgent.toLowerCase();
            if (myNav.indexOf('msie') != -1) {
                _ie_ver = parseInt(myNav.split('msie')[1]);
            } else if (myNav.indexOf("trident/") > 0) {
                _ie_ver = parseInt(myNav.split('trident/')[1]) + 4;
            } else {
                _ie_ver = false;
            }
        }

        return _ie_ver;
    };

    this.Clone = function (obj)
    {
        if (null == obj || !this.IsObject(obj))
        {
            return obj;
        }

        var copy = obj.constructor();

        for (var attr in obj)
        {
            if (obj.hasOwnProperty(attr))
                copy[attr] = obj[attr];
        }

        return copy;
    };

    this.GetElement = function (iElm)
    {
        if (this.IsString(iElm))
        {
            if (iElm.substr(0, 1) == "#")
                return $(iElm);

            return $("#" + iElm);
        }

        return iElm;
    };

    this.ElementExists = function (iElementID)
    {
        iElementID = this.GetElement(iElementID);

        return $(iElementID).length > 0;
    };

    this.Check = function (iCheckBox, iChecked)
    {
        iCheckBox = this.GetElement(iCheckBox);

        if (this.GetBoolean(iChecked))
            $(iCheckBox).attr("checked", "checked");
        else
            $(iCheckBox).removeAttr("checked");
    };

    this.IsChecked = function (iCheckBox)
    {
        iCheckBox = this.GetElement(iCheckBox);

        return iCheckBox.is(":checked");
    };

    this.GetRadioValue = function (iGroupName)
    {
        return $("input:radio[name=" + iGroupName + "]:checked").val();
    };

    this.GetComboText = function (iComboBox)
    {
        return $("#" + iComboBox + " option:selected").text();
    };

    this.GetComboIndex = function (iComboBox)
    {
        return $("#" + iComboBox).prop("selectedIndex");
    };

    this.SetComboIndex = function (iComboBox, iIndex)
    {
        $("#" + iComboBox).prop("selectedIndex", iIndex);
    };

    this.GetComboItemCount = function (iComboBox)
    {
        return $(this.Substitute("#{0} option", iComboBox)).length;
    };

    this.GetBoolean = function (iWannabeBool)
    {
        if (this.IsEmpty(iWannabeBool))
            return false;

        return true;
    };

    this.SetEnabled = function (iWho, iEnabled)
    {
        if (iEnabled)
            this.Enable(iWho);
        else
            this.Disable(iWho);
    };

    this.Enable = function (iWho)
    {
        iWho = this.GetElement(iWho);

        $(iWho).removeAttr("disabled");
    };

    this.Disable = function (iWho)
    {
        iWho = this.GetElement(iWho);

        $(iWho).attr("disabled", "disabled");
    };

    this.IsEnabled = function (iWho)
    {
        return !(this.HasAttr(iWho, "disabled") && $("#" + iWho).attr("disabled") == "disabled");
    };

    this.HasAttr = function (iWho, iAttr)
    {
        return !this.IsEmpty($("#" + iWho).attr(iAttr));
    };

    this.IsEqual = function (pDude1, pDude2)
    {
        if (this.IsEmpty(pDude1))
            return this.IsEmpty(pDude2);

        if (this.IsEmpty(pDude2))
            return this.IsEmpty(pDude1);

        return pDude1.toString().toLowerCase() == pDude2.toString().toLowerCase();
    };

    this.IsEmpty = function (iDude)
    {
        if (typeof iDude === "undefined" || iDude == undefined || iDude == null || iDude === "" || iDude === false || iDude === "false")
            return true;

        if (iDude instanceof Function || typeof iDude == 'function')
        {
            return false;
        }

        if (this.IsObject(iDude))
        {
            for (var prop in iDude) {
                if (iDude.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return true;
        }

        if (iDude instanceof Array)
        {
            return ($(iDude).length == 0);
        }

        var xStr = iDude.toString().toLowerCase();
        if (xStr == "0" || xStr == "false")
            return true;

        return false;
    };

    this.IsJSON = function(item) {
        item = (typeof item !== "string" ? JSON.stringify(item) : item);

        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        return !(Util.IsEmpty(item));
    };

    this.IsString = function (iDude)
    {
        return (typeof (iDude) == "string");
    };

    this.IsNumeric = function (iWannabeNumber)
    {
        return !isNaN(parseFloat(iWannabeNumber)) && isFinite(iWannabeNumber);
    };

    this.IsObject = function (val) {
        return (typeof val === 'object');
    };

    this.Substitute = function ()
    {
        var xStr = arguments[0];

        for (var xlp = 0; xlp < arguments.length - 1; xlp++)
        {
            var xReg = new RegExp("\\{" + xlp + "\\}", "gm");

            xStr = xStr.replace(xReg, arguments[xlp + 1]);
        }

        return xStr;
    };

    this.RegExpEscape= function(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    };
    this.IsVisible = function (iElementID)
    {
        iElementID = this.GetElement(iElementID);

        if (!this.ElementExists(iElementID))
            return false;

        if ($(iElementID).hasClass(".ui-dialog"))
            return $(iElementID).dialog("isOpen");

        if ($(iElementID).parent().hasClass("ui-dialog"))
            return $(iElementID).dialog("isOpen");

        return $(iElementID).is(":visible");
    };

    this.ObjectSize = function (iObject)
    {
        if (this.IsEmpty(iObject))
            return 0;

        if (iObject instanceof Array)
            return iObject.length;

        var xCnt = 0;
        var xDude;

        for (xDude in iObject)
        {
            if (iObject.hasOwnProperty(xDude))
                xCnt++;
        }

        return xCnt;
    };

    this.Log = function (data)
    {
        if (!_log_enabled)
        {
            return;
        }

        var myType = typeof data;

        if (!window.console)
        {
            window.console = {log: function () { }};
        }

        if (this.IEVersion() !== false && this.IEVersion() < 10 && (myType == "object" || myType == "array"))
        {
            console.log(JSON.stringify(data, null, "    "));
        } else

        {
            console.log(data);
        }
    };

    this.LogEnable = function (enableIt)
    {
        _log_enabled = enableIt;
    };

    this.OneOfTwo = function (iCond, iOut1, iOut2)
    {
        return this.GetBoolean(iCond) ? iOut1 : iOut2;
    };

    this.StartsWith = function (pString, pWhat)
    {
        pWhat = pWhat.toLowerCase();
        pString = pString.toLowerCase();

        return pString.substring(0, pWhat.length) === pWhat;
    };

    this.Contains = function (pString, pWhat)
    {
        if (this.IsEmpty(pString) || this.IsEmpty(pWhat))
            return false;

        return pString.indexOf(pWhat) > -1;
    };

    this.RemoveFromArray = function (pArray, pWhat)
    {
        if (this.IsEmpty(pArray))
            return;

        pArray.splice(pArray.indexOf(pWhat), 1);
    };

    this.MergeArrays = function (a1, a2)
    {
        return a1.concat(a2.filter(function (item) {
            return a1.indexOf(item) < 0;
        }));
    };

    this.ShowError = function (iEvt)
    {
        console.log(iEvt);
    };

    this.Explode = function (iStr, iDelimiter, iPairDelimiter) {
        if (!this.IsEmpty(iPairDelimiter))
        {
            xVars = iStr.split(iDelimiter);
            xNew = new Array();

            for (var lp = 0; lp < xVars.length; lp++)
            {
                xPair = xVars[lp].split(iPairDelimiter)
                xNew[xPair[0]] = xPair[1];
            }

            return xNew;
        } else

        {
            return iStr.split(iDelimiter);
        }
    };

    this.MakeURL = function (pWhere, pParms)
    {
        if (this.IsEmpty(pParms))
            return pWhere;

        if (!this.Contains(pWhere, "?"))
            pWhere += "?";

        if (this.IsString(pParms))
            return pWhere + pParms;

        var xP = "";

        for (var xKey in pParms)
        {
            xP += this.Substitute("&{0}={1}", xKey, pParms[xKey]);
        }

        return pWhere + xP.substr(1);       //  Remove the leading &
    };

    this.Money = function (value) {
        return '$' + parseFloat(value, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    };

    this.ScrollTop = function () {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {           
            window.scrollTo(0,0); // first value for left offset, second value for top offset
        } else {
            $('html,body').scrollTop("fast");
            /* .animate({
                scrollTop: 0,
                scrollLeft: 0
            }, "fast", function(){
                $('html,body').clearQueue();
            }); */
        }
    };

    this.ScrollTo = function(pElement, iSpeed) {
        if (this.IsVisible(pElement)) {
            $('html, body').animate({scrollTop: $(pElement).offset().top}, parseInt(iSpeed));
        }
    };

    this.ValidateEmail = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    this.Percent = function (value, decimals) {
        return this.Percentage(parseFloat(value, 10) * 100);
    };

    this.Percentage = function (value, decimals) {
        if (this.IsEmpty(decimals)) {
            decimals = 1;
        }
        return parseFloat(value, 10).toFixed(decimals).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + '%';
    };

    this.StripNonNumeric = function (value) {
        return value.replace(/[^\d.-]/g, '');
    };

    this.Phone = function (value) {
        if (!this.IsEmpty(value)) {
            return value.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '($1) $2 $3');
        } else {
            return '';
        }
    };

    /**
     *
     *
     * @param {String}
     * @returns first char capitalize
     */
    this.Capitalize = function (inString) {
        return inString.charAt(0).toUpperCase() + inString.slice(1);
    };

    this.Pad = function (inString, targetLength, padString) {
        targetLength = parseInt(targetLength);
        var targetString = String(inString);
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return targetString;
        } else {
            targetLength = targetLength - (targetString.length);
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + targetString;
        }
    };

    /**
     *
     *
     * @param {String} cname - cookie name want to set
     * @param {JSON} data
     * @param {Number} exdays - Amount of days tell cookie ends
     * @param {Any} raw
     */
    this.SetCookie = function (cname, data, exdays, raw) {
        if (Util.IsEmpty(raw)) {
            data = JSON.stringify(data);
        }
        var cookie;
        if (this.IsEmpty(exdays)) {
            cookie = [cname, '=', JSON.stringify(data), '; path=/;'].join('');
        } else {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = ";expires=" + d.toUTCString();
            cookie = [cname, '=', JSON.stringify(data), expires, '; path=/;'].join('');
        }
        document.cookie = cookie;
    };

    /**
     *
     *
     * @param {String} cname - Cookie name you want to get value for
     */
    this.GetCookie = function (cname) {
        var result = document.cookie.match(new RegExp(cname + '=([^;]+)'));
        if (result) {
            if (this.IsJSON(result[1])) {
                return JSON.parse(result[1]);
            } else {
                return result[1];
            }
        }

        return false;
    };

    /**
     *
     *
     * @param {String} cname - Cookie name you want to delete
     */
    this.DeleteCookie = function (cname) {
        document.cookie = [cname, '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/;'].join('');
    };

    this.urlParam = function (name, defValue) {
        if (this.IsEmpty(defValue)) {
            defValue = 0;
        }
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(window.location.href);
        if (!results) {
            return null;
        }
        if (!results[2]) {
            return defValue;
        }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    };

    /**
     * Fixed Header with clean tranistion between scroll
     * 
     * @param {Element} element - example $(".main")
     * @param {Number} scrollOffset - offset to trigger fixed header
     */
    this.FixedHeader = function(element, scrollOffset){
        if(element.length){
            var iScrollPos = 0;
            var offset = scrollOffset;
            
            window.onscroll = function() {
                var currentScroll = Util.GetCurrentScroll();
                
                if (currentScroll > offset) {
                    element.addClass("--unstick");
                } else {
                    element.removeClass("--unstick");
                }

                if (currentScroll < iScrollPos) element.removeClass("--unstick");

                iScrollPos = currentScroll;
            };
        }
    };

    /**
     * Fixed Header with nice scrolling color change :)
     * 
     * @param {Element} element - example $(".main")
     */
    this.FixedHeaderAnimate = function(element){
        if(element.length){
            window.onscroll = function() {
                var scrollPos = Util.GetCurrentScroll(), 
                targetOpacity = 1;
                scrollPos < 200 ? targetOpacity = ((scrollPos * .5 / 100)) : 1;
                element.css({
                    'background-color': 'rgba(0, 0, 0, ' + targetOpacity + ')'
                });
            };
        }
    };

    /**
     * 
     * 
     * @returns Current Scroll Y Offset
     */
    this.GetCurrentScroll = function () {
        return window.pageYOffset || document.documentElement.scrollTop;
    };

});
