/**
 * Alert Message Object
 * Make sure to Include *** juqery.min.js ***
 * Make sure to Include *** Util.js ***
 * Make sure to Include *** bootstrap4.js ***
 */
var AlertMessage = new (function AlertMessage() {
    var _timer = null;
    var _self = this; // use _self to access scope restrictions with 'this'
    var _timeout = 10000; //Default Timeout timer can change to how long you want or set AlertMessage.setNewTimeout(false); for no timeout

    // Events
    $("#confirm-modal .btn-confirm").click(function () {
        $(document).trigger("alert.confirm.confirmed", [$("#confirm-modal").data("confirm-target")]);
        $("#confirm-modal").modal("hide");
    });
    $("#confirm-modal .btn-close").click(function () {
        $(document).trigger("alert.confirm.closed");
    });
    
    /*
    this.ajaxFormErrorHandler = function (ev, response, form) {
            Util.Log(response);
        switch (response.Status.toString().toUpperCase()) {
            case "ERROR":
            case "INVALID":
                _self.addMessage(response.Message, "danger");
                break;
        }
    };
    
    
    this.ajaxFormMessageHandler = function (ev, response, form) {
        switch (response.Status.toString().toUpperCase()) {
            case "GOOD":
                _self.addMessage(response.Message, "success");
                break;
            case "ERROR":
            case "INVALID":
                _self.addMessage(response.Message, "danger");
                break;
            case "NODATA":
                _self.addMessage(response.Message, "primary");
                break;
        }
    }; */

    // public methods
    this.addMessage = function (message, type, target) {
        _self.clear();

        Util.scrollTop();

        if (Util.isEmpty(type)) {
            type = 'primary';
        }

        if (type === 'confirm') {
            $("#confirm-modal .modal-body p").html(message);
            $("#confirm-modal").data("confirm-target", target);
            $("#confirm-modal").modal({ show: true, closeByBackdrop: false, closeByKeyboard: false });
        } else {
            var $alertTop = $("#alertTop");
            var $newAlert = $alertTop.find(".alert-template").clone();

            $newAlert.removeClass("alert-template").addClass("alert-" + type.toString().toLowerCase()).find(".message").html(message);
            $alertTop.append($newAlert);
            $newAlert.show();

             _self.setTimeout();
        }
    };

    this.setNewTimeout = function (newTimeout) {
       _timeout = newTimeout;   
    };

    this.setTimeout = function () {
        if(_timeout !== false){
            _timer = setTimeout(function () {
                _self.clear();
            }, _timeout);
        }
    };

    this.clear = function () {
        $("#alertTop .alert:not(.alert-template)").remove();
        _timer = null;
    };
});